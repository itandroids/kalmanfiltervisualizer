#ifndef FIELDOBJECT_H
#define FIELDOBJECT_H

#include <QGraphicsObject>


class FieldObject : public QGraphicsObject
{
public:
    FieldObject(QGraphicsItem* parent = nullptr);
};

#endif // FIELDOBJECT_H
