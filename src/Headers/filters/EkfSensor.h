#ifndef EKFSENSOR_H
#define EKFSENSOR_H

#include "Headers/filters/AbstractSensor.h"
#include "Headers/filters/Simulator.h"

class EkfSensor : public AbstractSensor
{
public:
    EkfSensor(Simulator* simulator, double latency,
              double sigmaCameraCartesian, double sigmaCameraAngle,
              QGraphicsItem* parent = nullptr);

private:
    void updateReadings() override;

private:
    Simulator* sim;
    double sigmaCamCart;
    double sigmaCamAngle;
};

#endif // EKFSENSOR_H
