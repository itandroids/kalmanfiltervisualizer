//
// Created by mmaximo on 3/17/18.
//

#ifndef KALMAN_FILTERING_BALLSIMULATOR_H
#define KALMAN_FILTERING_BALLSIMULATOR_H

#include "Headers/math/Vector2.h"
#include "Headers/math/Pose2D.h"
#include <queue>

class BallSimulator {
public:
    BallSimulator(double sigmaAcceleration, double sigmaCamera, double T, int cameraDelay = 0);

    void update();

    math::Pose2D observe() const;

    const math::Pose2D& getPosition() const;

    const math::Pose2D& getVelocity() const;

    void setVelocity(const math::Pose2D& velocity);

private:
    double sigmaAcceleration;
    double sigmaCamera;
    double T;
    int cameraDelay;
    std::queue<math::Pose2D> observations;
    math::Pose2D position;
    math::Pose2D velocity;
};


#endif //KALMAN_FILTERING_BALLSIMULATOR_H
