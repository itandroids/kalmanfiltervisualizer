//
// Created by mmaximo on 3/16/18.
//

#ifndef KALMAN_FILTERING_ROBOT2DSIMULATOR_H
#define KALMAN_FILTERING_ROBOT2DSIMULATOR_H

#include "Headers/math/Vector2.h"
#include "Headers/math/Pose2D.h"
#include "Headers/filters/Simulator.h"

class Robot2DSimulator : public Simulator {
public:
    Robot2DSimulator(double sigmaMotion, double sigmaGPS, double T);

    void update(const math::Vector2d& velocity) override;

    math::Pose2D observe() const override;

    const math::Pose2D& getPose() const override;

private:
    double sigmaMotion;
    double sigmaGPS;
    double T;
    math::Pose2D position;
};


#endif //KALMAN_FILTERING_ROBOT2DSIMULATOR_H
