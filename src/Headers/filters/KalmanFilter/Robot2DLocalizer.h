//
// Created by mmaximo on 3/16/18.
//

#ifndef KALMAN_FILTERING_ROBOT2DLOCALIZER_H
#define KALMAN_FILTERING_ROBOT2DLOCALIZER_H

#include <Eigen/Core>
#include <memory>
#include "Headers/math/Vector2.h"
#include "Headers/math/Pose2D.h"
#include "Headers/filters/Localizer.h"
#include "KalmanFilter.h"

//namespace signal_processing {

/**
 * Localizer for a 2D robot with GPS.
 */
class Robot2DLocalizer : public Localizer {
public:
    /**
     * Constructs a 2D robot localizer.
     * @param sigmaMotion standard deviation of the velocity command.
     * @param sigmaGPS standard deviation of the GPS measurements.
     * @param T sample time.
     */
    Robot2DLocalizer(double sigmaMotion, double sigmaGPS, double T);

    /**
     * Resets the localizer to a given position and covariance.
     * @param resetPosition position to reset the localizer to.
     * @param resetCovariance covariance to reset the localizer to.
     */
    void reset(const math::Pose2D &resetPosition, const Eigen::MatrixXd &resetCovariance);

    /**
     * Executes a prediction step.
     * @param command velocity command.
     * @return new predicted state.
     */
    const math::Pose2D &predict(const math::Vector2d &command);

    /**
     * Executes a observation step.
     * @param observation GPS observation.
     * @return new filtered state.
     */
    const math::Pose2D &filter(const math::Pose2D &observation);

    /**
     * Obtains the current position estimate.
     * @return current position estimate.
     */
    const math::Pose2D &getPose() const;

    /**
     * Obtains the current position uncertainty.
     * @return current position uncertainty.
     */
    const Eigen::MatrixXd &getCovariance() const;

private:
    std::shared_ptr<KalmanFilter> kalmanFilter;
    math::Pose2D position;
};

//}

#endif //KALMAN_FILTERING_ROBOT2DLOCALIZER_H
