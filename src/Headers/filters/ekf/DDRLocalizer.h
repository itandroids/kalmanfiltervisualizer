//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_DDRLOCALIZER_H
#define EKF_DDRLOCALIZER_H

#include <Eigen/Dense>
#include <memory>
#include "Headers/math/Pose2D.h"
#include "NonlinearModel.h"
#include "Headers/filters/Localizer.h"

class DDRLocalizer : public Localizer {
public:
    DDRLocalizer(double T, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian, double sigmaCameraAngle);

    void reset(const math::Pose2D& pose, const Eigen::MatrixXd &covariance) override;

    const math::Pose2D &predict(const math::Vector2d& command) override;

    const math::Pose2D &filter(const math::Pose2D& observation) override;

    const Eigen::VectorXd& getState();

    const math::Pose2D &getPose() const override;

    const Eigen::MatrixXd& getCovariance() const override;

private:
    std::shared_ptr<NonlinearModel> model;
    Eigen::VectorXd state;
    Eigen::MatrixXd covariance;
    math::Pose2D pose;
};


#endif //EKF_DDRLOCALIZER_H
