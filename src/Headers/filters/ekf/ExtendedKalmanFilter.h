//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_EXTENDEDKALMANFILTER_H
#define EKF_EXTENDEDKALMANFILTER_H

#include <memory>
#include "NonlinearModel.h"

class ExtendedKalmanFilter {
public:
    ExtendedKalmanFilter(std::shared_ptr<NonlinearModel> model);

    const Eigen::VectorXd& predict(const Eigen::VectorXd& command);

    const Eigen::VectorXd& filter(const Eigen::VectorXd& observation);

    const Eigen::VectorXd& getState();

    const Eigen::MatrixXd& getCovariance();

private:
    std::shared_ptr<NonlinearModel> model;
    Eigen::VectorXd state;
    Eigen::MatrixXd covariance;
};


#endif //EKF_EXTENDEDKALMANFILTER_H
