//
// Created by mmaximo on 3/24/18.
//

#ifndef EKF_DDRSIMULATOR_H
#define EKF_DDRSIMULATOR_H

#include <memory>
#include <Eigen/Dense>
#include "NonlinearModel.h"
#include "Headers/math/Pose2D.h"
#include "Headers/filters/Simulator.h"

class DDRSimulator : public Simulator {
public:
    DDRSimulator(double T, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian, double sigmaCameraAngle);

    void update(const math::Vector2d& velocity) override;

    math::Pose2D observe() const override;

    const math::Pose2D& getPose() const override;

private:
    math::Pose2D pose;
    double T;
    double sigmaLinearSpeed;
    double sigmaAngularSpeed;
    double sigmaCameraCartesian;
    double sigmaCameraAngle;
};


#endif //EKF_DDRSIMULATOR_H
