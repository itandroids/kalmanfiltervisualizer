#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "Headers/math/Vector2.h"
#include "Headers/math/Pose2D.h"

class Simulator {
public:
    Simulator() {}
    virtual ~Simulator() = default;

    virtual void update(const math::Vector2d& velocity) = 0;

    virtual math::Pose2D observe() const = 0;

    virtual const math::Pose2D& getPose() const = 0;

};

#endif // SIMULATOR_H
