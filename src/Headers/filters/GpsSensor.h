#ifndef GPSSENSOR_H
#define GPSSENSOR_H

#include "AbstractSensor.h"

class GpsSensor : public AbstractSensor
{
    Q_OBJECT
public:
    GpsSensor(double latency, double sigmaGps, QGraphicsItem* parent = nullptr);

private:
    void updateReadings() override;
    double sigmaGps;
};

#endif // GPSSENSOR_H
