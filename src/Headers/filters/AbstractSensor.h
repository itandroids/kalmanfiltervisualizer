#ifndef ABSTRACTSENSOR_H
#define ABSTRACTSENSOR_H

#include <QGraphicsObject>
#include "Headers/FieldObject.h"
#include <QtWidgets>


class AbstractSensor : public QGraphicsObject
{
    Q_OBJECT
public:
    AbstractSensor(double sensorDT, QGraphicsItem* parent = nullptr);

    QRectF boundingRect() const override;

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
               QWidget* widget = nullptr) override;

    void addFlag(FieldObject* flag);

    void addTrackTarget(FieldObject* target);
signals:
    virtual void infoReady(QList<QVector3D> flagsReadings, QVector3D trackedReading,
                   QVector3D parentReading);
public slots:
    void update();

private:
    virtual void updateReadings() = 0;
protected:
    QList<FieldObject*> flags;
    FieldObject* trackTarget;
    QTimer* latencyTimer;

    QList<QVector3D> flagsReadings;
    QVector3D trackedReading;
    QVector3D parentReading;

};

#endif // ABSTRACTSENSOR_H
