#ifndef LOCALIZER_H
#define LOCALIZER_H

#include <Eigen/Core>
#include <memory>
#include "Headers/math/Vector2.h"
#include "Headers/math/Pose2D.h"

//namespace signal_processing {

/**
 * Localizer for a 2D robot with GPS.
 */
class Localizer {
public:
    /**
     * Constructs a 2D robot localizer.
     * @param sigmaMotion standard deviation of the velocity command.
     * @param sigmaGPS standard deviation of the GPS measurements.
     * @param T sample time.
     */
    Localizer() { }
    virtual ~Localizer() = default;

    /**
     * Resets the localizer to a given position and covariance.
     * @param resetPosition position to reset the localizer to.
     * @param resetCovariance covariance to reset the localizer to.
     */
    virtual void reset(const math::Pose2D &resetPosition, const Eigen::MatrixXd &resetCovariance) = 0;

    /**
     * Executes a prediction step.
     * @param command velocity command.
     * @return new predicted state.
     */
    virtual const math::Pose2D &predict(const math::Vector2d &command) = 0;

    /**
     * Executes a observation step.
     * @param observation GPS observation.
     * @return new filtered state.
     */
    virtual const math::Pose2D &filter(const math::Pose2D &observation) = 0;

    /**
     * Obtains the current position estimate.
     * @return current position estimate.
     */
    virtual const math::Pose2D &getPose() const = 0;

    /**
     * Obtains the current position uncertainty.
     * @return current position uncertainty.
     */
    virtual const Eigen::MatrixXd &getCovariance() const = 0;

};

#endif // LOCALIZER_H
