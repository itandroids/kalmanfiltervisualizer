#ifndef ROBOT_H
#define ROBOT_H

#include "RobotPart.h"
#include "Arrow.h"
#include "Headers/filters/AbstractSensor.h"
#include "Headers/filters/KalmanFilter/Robot2DLocalizer.h"
#include "Headers/filters/KalmanFilter/Robot2DSimulator.h"
#include "Headers/math/Vector2.h"
#include "RobotBuilder.h"


class RobotBuilder;
class KFRobotBuilder;
class EKFRobotBuilder;

class Robot : public RobotPart
{
    Q_OBJECT
    friend class RobotBuilder;
    friend class KFRobotBuilder;
    friend class EKFRobotBuilder;
public:
    static RobotBuilder create(QGraphicsPathItem* sensorPath = nullptr,
                               QGraphicsPathItem* filterPath = nullptr,
                               QGraphicsPathItem* selfPath = nullptr,
                               QGraphicsItem* parent = nullptr) {
        return RobotBuilder(sensorPath, filterPath, selfPath, parent);
    }

    ~Robot();

    static math::Pose2D qVectorToPose(const QVector3D& v);
protected:
    Robot(QGraphicsPathItem* sensorPath = nullptr,
          QGraphicsPathItem* filterPath = nullptr,
          QGraphicsPathItem* selfPath = nullptr,
          QGraphicsItem* parent = nullptr);

    QRectF boundingRect() const override;

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
               QWidget* widget = nullptr) override;

    QPainterPath shape() const override;
    QVariant itemChange(QGraphicsItem::GraphicsItemChange change,
                                       const QVariant &value) override;
public slots:
    void receiveReading(QList<QVector3D> flagsReadings, QVector3D trackedReading,
                   QVector3D parentReading);

    void move() ;

protected:


private:

    void updateShape();


    QVector3D poseToQVector(const math::Pose2D& v);
private:
    QPixmap pixmap;
    QPointF position;
    QPointF offset;
    QRectF bounding;
    Arrow* rotatorArrow;
    bool hasShape;
    QPainterPath robotShape;
    AbstractSensor* sensor;
    Localizer* localizer;
    Simulator* simulator;
    QVector3D readPos;
    QTimer* latencyTimer;
    QGraphicsPathItem* sensorPath;
    QGraphicsPathItem* selfPath;
    QGraphicsPathItem* filterPath;
    std::function<math::Pose2D(Simulator*)> moveController;
    std::function<math::Pose2D(Localizer*, QList<QVector3D>, QVector3D, QVector3D)> sensorRcvController;
    unsigned int stepCounter = 0;
    unsigned int pathLineLimit = 200;

};

#endif // ROBOT_H
