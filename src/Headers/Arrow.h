#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsLineItem>

class QGraphicsPolygonItem;
class QGraphicsLineItem;
class QGraphicsScene;
class QRectF;
class QGraphicsSceneMouseEvent;
class QPainterPath;

class Arrow : public QGraphicsLineItem
{
public:
    enum { Type = UserType + 4 };

    Arrow(QGraphicsItem *origin, QGraphicsItem *parent = 0);

    int type() const override { return Type; }
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void setColor(const QColor &color) { myColor = color; }
    QGraphicsItem *startItem() const { return myStartItem; }

    void updatePosition();

//protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent  *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent  *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent  *event) override;

private:
    QGraphicsItem *myStartItem;
    QColor myColor;
    QPolygonF arrowHead;
};

#endif // ARROW_H
