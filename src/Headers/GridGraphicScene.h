#ifndef CUSTOMQGRAPHICSSCENE_H
#define CUSTOMQGRAPHICSSCENE_H
#include <QGraphicsScene>
 
class GridGraphicScene : public QGraphicsScene
{
public:
   GridGraphicScene(QObject *parent);
 
protected:
   void drawBackground(QPainter * painter, const QRectF & rect ) override;
};
 
#endif // CUSTOMQGRAPHICSSCENE_H
