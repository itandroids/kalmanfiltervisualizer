#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include "Robot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    void createSideBar();
public slots:
    void constructRobot();
    void stopRobot();
private:
    Ui::MainWindow *ui;
    QGraphicsScene* scene;
    Robot* robot = nullptr;
    QDoubleSpinBox* latencySpin;
    QDoubleSpinBox* sigmaMotionSpin;
    QDoubleSpinBox* sigmaGpsSpin;

    QDoubleSpinBox* sigmaLinSpdSpin;
    QDoubleSpinBox* sigmaAngSpin;
    QDoubleSpinBox* sigmaCamCartSpin;
    QDoubleSpinBox* sigmaCamAngSpin;

    QDoubleSpinBox* ukfSigmaLinSpdSpin;
    QDoubleSpinBox* ukfSigmaAngSpin;
    QDoubleSpinBox* ukfSigmaCamCartSpin;
    QDoubleSpinBox* ukfSigmaCamAngSpin;

    QDoubleSpinBox* pfSigmaLinSpdSpin;
    QDoubleSpinBox* pfSigmaAngSpin;
    QDoubleSpinBox* pfSigmaCamCartSpin;
    QDoubleSpinBox* pfSigmaCamAngSpin;

    QGraphicsPathItem* sensorPath = nullptr;
    QGraphicsPathItem* robotPath = nullptr;
    QGraphicsPathItem* filterPath = nullptr;
    QVBoxLayout* sideBarLayout;
    QComboBox* robotMovementModelBox;
    QComboBox* robotSensorTypeBox;
    QComboBox* robotFilterTypeBox;
    QComboBox* ballMovementModelBox;
    QComboBox* ballSensorTypeBox;
    QComboBox* ballFilterTypeBox;
    QPushButton* start;
    QPushButton* stop;
    QStackedWidget* localizerTrackerStack;
    QStackedWidget* localizerConfigStack;
    QStackedWidget* ballConfigStack;

};

#endif // MAINWINDOW_H
