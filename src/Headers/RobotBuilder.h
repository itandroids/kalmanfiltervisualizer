#ifndef ROBOTBUILDER_H
#define ROBOTBUILDER_H

#include <QtWidgets>

class Robot;
class KFRobotBuilder;
class EKFRobotBuilder;

class RobotBuilderBase {
protected:
    Robot* robot;

    explicit RobotBuilderBase(Robot* robot) : robot(robot) {}

public:
    operator Robot*() {
        return robot;
    }

    KFRobotBuilder constructKFRobot() const;
    EKFRobotBuilder constructEKFRobot() const;
};

class RobotBuilder : public RobotBuilderBase
{
public:
     RobotBuilder(QGraphicsPathItem* sensorPath = nullptr,
                  QGraphicsPathItem* filterPath = nullptr,
                  QGraphicsPathItem* selfPath = nullptr,
                  QGraphicsItem* parent = nullptr);
};

class KFRobotBuilder : public RobotBuilderBase {
    using self = KFRobotBuilder;
public:
    explicit KFRobotBuilder(Robot* robot);

    self& withParameters(double dT, double sigmaGps, double sigmaMotion);

};

class EKFRobotBuilder : public RobotBuilderBase {
    using self = EKFRobotBuilder;
public:
    explicit EKFRobotBuilder(Robot* robot);

    self& withParameters(double dT,
                         double sigmaLinearSpeed,
                         double sigmaAngularSpeed,
                         double sigmaCameraCartesian,
                         double sigmaCameraAngle);

};

#endif // ROBOTBUILDER_H
