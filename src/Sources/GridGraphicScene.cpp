#include "Headers/GridGraphicScene.h"
#include <QPainter>
 
static const int GRID_STEP = 30;
 
inline qreal round(qreal val, int step) {
   int tmp = int(val) + step /2;
   tmp -= tmp % step;
   return qreal(tmp);
}
 
GridGraphicScene::GridGraphicScene(QObject *parent ) : QGraphicsScene(parent)
{}
 
void GridGraphicScene::drawBackground(QPainter *painter, const QRectF &rect)
{
   int step = GRID_STEP;

   painter->setPen(QPen(Qt::red));
   painter->drawLine(0.0, rect.top(), 0.0, rect.bottom());
   painter->setPen(QPen(Qt::blue));
   painter->drawLine(rect.left(), 0.0, rect.right(), 0.0);

   painter->setPen(QPen(QColor(0, 200, 0, 225)));
   painter->setFont(QFont("times",8));
   // draw horizontal grid
   qreal start = round(rect.top(), step);
   if (start > rect.top()) {
      start -= step;
   }
   for (qreal y = start - step; y < rect.bottom(); ) {
      y += step;
      int label = -static_cast<int>(y)/step;
      if (label != 0)
          painter->drawLine(rect.left(), y, rect.right(), y);
      painter->setPen(QPen(Qt::black));
      painter->drawText(-18, y, QString::number(label)+QString(".0"));
      painter->setPen(QPen(QColor(0, 200, 0, 225)));
   }
   // now draw vertical grid
   start = round(rect.left(), step);
   if (start > rect.left()) {
      start -= step;
   }
   for (qreal x = start - step; x < rect.right(); ) {
      x += step;
      int label = static_cast<int>(x)/step;
      if (label != 0)
          painter->drawLine(x, rect.top(), x, rect.bottom());

      painter->setPen(QPen(Qt::black));
      painter->drawText(x, 10, QString::number(label)+QString(".0"));
      painter->setPen(QPen(QColor(0, 200, 0, 225)));
   }

}
