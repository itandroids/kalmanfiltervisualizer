#include <QtWidgets>
#include "Headers/Robot.h"
#include <QDebug>
#include "Headers/filters/GpsSensor.h"
#include "Headers/filters/ekf/DDRLocalizer.h"
#include "Headers/filters/ekf/DDRSimulator.h"
#include "Headers/filters/EkfSensor.h"

Robot::Robot(QGraphicsPathItem* sensorPath,
             QGraphicsPathItem* filterPath,
             QGraphicsPathItem* selfPath,
             QGraphicsItem* parent)
    : RobotPart(parent), hasShape(false),
      filterPath(filterPath), sensorPath(sensorPath), selfPath(selfPath)
{
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    setFlag(GraphicsItemFlag::ItemIsMovable, true);
    this->setRotation(0);

    position = QPointF(0.0, 0.0);
    bounding = QRectF(-20, -20, 40, 40);
    pixmap.load(":/resources/robo.png");
    auto w = pixmap.size().width();
    auto h = pixmap.size().height();
    offset = QPointF(-w/2.0, -h/2.0);
    auto pixmapScaled = pixmap.scaled(static_cast<int>(w), static_cast<int>(h),
                                       Qt::KeepAspectRatio).toImage();

    rotatorArrow = new Arrow(this, this);
    rotatorArrow->setFlag(GraphicsItemFlag::ItemIsMovable, true);

    updateShape();

}

Robot::~Robot()
{
    delete rotatorArrow;
    delete sensor;
    delete localizer;
    delete simulator;
    delete latencyTimer;
}

QRectF Robot::boundingRect() const
{
    return robotShape.boundingRect();
}

void Robot::updateShape() {
    QBitmap mask = pixmap.mask();
    QRegion region(QRegion(mask).translated(offset.toPoint()));
    robotShape.addRegion(region);
    hasShape = true;
}

void Robot::move()
{
    math::Pose2D pos = moveController(simulator);
    QPointF newPos(pos.translation.x, pos.translation.y);
    this->setPos({pos.translation.x*30, -pos.translation.y*30});
    this->setRotation(-90-pos.rotation*180/M_PI);
    if (selfPath) {
        auto path = selfPath->path();
        if (stepCounter > pathLineLimit) {
            QPainterPath newPath;
            path = newPath;
            path.moveTo(this->pos());
            stepCounter = 0;
        }
        path.lineTo(this->pos());
        selfPath->setPath(path);
    }
    ++stepCounter;

}

math::Pose2D Robot::qVectorToPose(const QVector3D &v)
{
   return math::Pose2D(v.z(), v.x(), v.y());
}

QVector3D Robot::poseToQVector(const math::Pose2D &v)
{
   return QVector3D(v.translation.x, v.translation.y, v.rotation);
}

QPainterPath Robot::shape() const {
    return robotShape;
}

QVariant Robot::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemScaleChange || change == ItemScaleHasChanged) {
        rotatorArrow->setScale(5.0);
    }
    return QGraphicsItem::itemChange(change, value);
}

void Robot::receiveReading(QList<QVector3D> flagsReadings, QVector3D trackedReading,
                           QVector3D parentReading)
{
    math::Pose2D observation = qVectorToPose(parentReading);
    math::Pose2D position = sensorRcvController(localizer, flagsReadings, trackedReading,
                                                parentReading);
    readPos = 30*QVector2D(position.translation.x, -position.translation.y);

    if(filterPath) {
        auto path = filterPath->path();
        if (stepCounter > pathLineLimit) {
            QPainterPath newPath;
            path = newPath;
            path.moveTo(this->pos());
        }
        path.addEllipse({30*observation.translation.x, -30*observation.translation.y},
                        2,2);
        filterPath->setPath(path);
    }

    if(sensorPath) {
        auto path = sensorPath->path();
        if (stepCounter > pathLineLimit) {
            QPainterPath newPath;
            path = newPath;
            path.moveTo(this->pos());
        }
        path.lineTo(readPos.toPointF());
        sensorPath->setPath(path);
    }
}

void Robot::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
            QWidget* widget) {

    Q_UNUSED(option);
    Q_UNUSED(widget);

    auto w = bounding.width();
    auto h = bounding.height();
    QPointF offset(-w/2, -h/2);
    auto pixmapScaled = pixmap.scaled(static_cast<int>(w), static_cast<int>(h),
                                       Qt::KeepAspectRatio);
    painter->setRenderHint(QPainter::SmoothPixmapTransform, true);

    painter->drawPixmap(offset, pixmapScaled);
}
