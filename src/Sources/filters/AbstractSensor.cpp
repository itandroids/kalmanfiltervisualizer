#include "Headers/filters/AbstractSensor.h"

AbstractSensor::AbstractSensor(double sensorDT, QGraphicsItem* parent)
    :QGraphicsObject (parent)
{
    latencyTimer = new QTimer(this);

    connect(latencyTimer, SIGNAL(timeout()), this, SLOT(update()));
    int latency = static_cast<int>(round(sensorDT*1000));

    latencyTimer->start(latency);
}

QRectF AbstractSensor::boundingRect() const
{
   return QRectF();
}

void AbstractSensor::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}

void AbstractSensor::addFlag(FieldObject *flag)
{
    flags.push_back(flag);

}

void AbstractSensor::addTrackTarget(FieldObject *target)
{
   trackTarget = target;
}

void AbstractSensor::update()
{
   updateReadings();

   emit infoReady(flagsReadings, trackedReading, parentReading);
}

