#include "Headers/filters/EkfSensor.h"
#include "Headers/math/RandomUtils.h"
#include "Headers/math/MathUtils.h"

EkfSensor::EkfSensor(Simulator* simulator, double latency,
                     double sigmaCameraCartesian, double sigmaCameraAngle,
                     QGraphicsItem* parent)
    : AbstractSensor(latency, parent), sim(simulator), sigmaCamCart(sigmaCameraCartesian),
      sigmaCamAngle(sigmaCameraAngle)
{

}

void EkfSensor::updateReadings() {
    math::Pose2D observation = sim->getPose();
    observation.translation.x += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamCart);
    observation.translation.y += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamCart);
    observation.rotation += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamAngle);
    observation.rotation = math::MathUtils::normalizeAngle(observation.rotation);
    this->parentReading = QVector3D(observation.translation.x,
                                    observation.translation.y,
                                    observation.rotation);

}
