//
// Created by mmaximo on 3/24/18.
//

#include "Headers/filters/ekf/DDRLocalizer.h"

#include "Headers/filters/ekf/DDRModel.h"
#include "Headers/math/MathUtils.h"

DDRLocalizer::DDRLocalizer(double T, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian,
                           double sigmaCameraAngle)
    : Localizer() {
    state.resize(3);
    covariance.resize(3, 3);
        Eigen::MatrixXd M(2, 2);
    M << sigmaLinearSpeed * sigmaLinearSpeed, 0.0,
        0.0, sigmaAngularSpeed * sigmaAngularSpeed;
    Eigen::MatrixXd R(3, 3);
    R << sigmaCameraCartesian * sigmaCameraCartesian, 0.0, 0.0,
        0.0, sigmaCameraCartesian * sigmaCameraCartesian, 0.0,
        0.0, 0.0, sigmaCameraAngle * sigmaCameraAngle;
    model = std::make_shared<DDRModel>(T, M, R);
}

void DDRLocalizer::reset(const math::Pose2D &pose, const Eigen::MatrixXd &covariance) {
    state(0) = pose.translation.x;
    state(1) = pose.translation.y;
    state(2) = pose.rotation;
    this->pose = pose;
    this->covariance = covariance;
}

const math::Pose2D &DDRLocalizer::predict(const math::Vector2d& com) {
    Eigen::VectorXd command(2);
    command << com.x, com.y;
    Eigen::MatrixXd A = model->getA(state, command);
    Eigen::MatrixXd Q = model->getQ(state, command);
    state = model->propagate(state, command);
    state(2) = math::MathUtils::normalizeAngle(state(2));
    covariance = A * covariance * A.transpose() + Q;
    pose.translation.x = state(0);
    pose.translation.y = state(1);
    pose.rotation = state(2);
    return pose;
}

const math::Pose2D &DDRLocalizer::filter(const math::Pose2D& observation) {
    Eigen::VectorXd z(3);
    z << observation.translation.x, observation.translation.y, observation.rotation;
    Eigen::MatrixXd C = model->getC(state);
    Eigen::MatrixXd R = model->getR(state);
    Eigen::VectorXd expectedObservation = model->observe(state);
    Eigen::MatrixXd K = covariance * C.transpose() * (C * covariance * C.transpose() + R).inverse();
    Eigen::VectorXd error = z - expectedObservation;
    error(2) = math::MathUtils::normalizeAngle(error(2));
    state = state + K * error;
    covariance = covariance - K * C * covariance;
    pose.translation.x = state(0);
    pose.translation.y = state(1);
    pose.rotation = state(2);
    return pose;
}

const Eigen::VectorXd &DDRLocalizer::getState() {
    return state;
}

const math::Pose2D &DDRLocalizer::getPose() const {
    return pose;
}

const Eigen::MatrixXd &DDRLocalizer::getCovariance() const  {
    return covariance;
}

