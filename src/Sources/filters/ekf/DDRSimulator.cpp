//
// Created by mmaximo on 3/24/18.
//

#include "Headers/filters/ekf/DDRSimulator.h"
#include "Headers/math/RandomUtils.h"
#include "Headers/math/MathUtils.h"

DDRSimulator::DDRSimulator(double T, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian,
                           double sigmaCameraAngle) : Simulator(),
                                                      T(T), sigmaLinearSpeed(sigmaLinearSpeed),
                                                      sigmaAngularSpeed(sigmaAngularSpeed),
                                                      sigmaCameraCartesian(sigmaCameraCartesian),
                                                      sigmaCameraAngle(sigmaCameraAngle),
                                                      pose(math::Pose2D(0.0, 0.0, 0.0)) {
}

void DDRSimulator::update(const math::Vector2d& velocity) {
    double vn = velocity.x + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaLinearSpeed);
    double wn = velocity.y + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaAngularSpeed);
    if (fabs(wn) < 1.0e-3) {
        pose.translation.x = pose.translation.x + vn * T * cos(pose.rotation + wn * T / 2.0);
        pose.translation.y = pose.translation.y + vn * T * sin(pose.rotation + wn * T / 2.0);
    } else {
        pose.translation.x = pose.translation.x + 2.0 * (vn / wn) * cos(pose.rotation + wn * T / 2.0) * sin(wn * T / 2.0);
        pose.translation.y = pose.translation.y + 2.0 * (vn / wn) * sin(pose.rotation + wn * T / 2.0) * sin(wn * T / 2.0);
    }
    pose.rotation = math::MathUtils::normalizeAngle(pose.rotation + wn * T);
}

math::Pose2D DDRSimulator::observe() const {
    math::Pose2D observation = pose;
    observation.translation.x += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCameraCartesian);
    observation.translation.y += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCameraCartesian);
    observation.rotation += math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCameraAngle);
    observation.rotation = math::MathUtils::normalizeAngle(observation.rotation);
    return observation;
}

const math::Pose2D &DDRSimulator::getPose() const {
    return pose;
}
