//
// Created by mmaximo on 3/24/18.
//

#include "Headers/filters/ekf/ExtendedKalmanFilter.h"

ExtendedKalmanFilter::ExtendedKalmanFilter(std::shared_ptr<NonlinearModel> model) : model(model) {
    state.resize(3);
    covariance.resize(3, 3);
}

const Eigen::VectorXd &ExtendedKalmanFilter::predict(const Eigen::VectorXd &command) {
    Eigen::MatrixXd A = model->getA(state, command);
    Eigen::MatrixXd Q = model->getQ(state, command);
    state = model->propagate(state, command);
    covariance = A * covariance * A.transpose() + Q;
    return state;
}

const Eigen::VectorXd &ExtendedKalmanFilter::filter(const Eigen::VectorXd &observation) {
    Eigen::MatrixXd C = model->getC(state);
    Eigen::MatrixXd R = model->getR(state);
    Eigen::VectorXd expectedObservation = model->observe(state);
    Eigen::MatrixXd K = C * covariance * C.transpose() + R;
    state = state + K * (observation - expectedObservation);
    covariance = covariance - K * C * covariance;
    return state;
}

const Eigen::VectorXd &ExtendedKalmanFilter::getState() {
    return state;
}

const Eigen::MatrixXd &ExtendedKalmanFilter::getCovariance() {
    return covariance;
}
