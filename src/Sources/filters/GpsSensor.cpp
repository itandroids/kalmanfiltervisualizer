#include "Headers/filters/GpsSensor.h"
#include "Headers/math/RandomUtils.h"

GpsSensor::GpsSensor(double latency, double sigmaGps, QGraphicsItem* parent)
    : AbstractSensor(latency, parent), sigmaGps(sigmaGps)
{

}

void GpsSensor::updateReadings()
{
    if (this->parentItem()) {
        auto pos = this->parentItem()->pos();
        auto x = pos.x()/30+ math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaGps);
        auto y = -pos.y()/30+ math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaGps);
        this->parentReading = QVector3D(x, y, 0.0);
    }

}
