//
// Created by mmaximo on 3/16/18.
//

#include "Headers/filters/KalmanFilter/Robot2DSimulator.h"
#include "Headers/math/RandomUtils.h"

Robot2DSimulator::Robot2DSimulator(double sigmaMotion, double sigmaGPS, double T)
    : Simulator (), sigmaMotion(sigmaMotion),
      sigmaGPS(sigmaGPS), T(T), position(0.0, 0.0) {
}

void Robot2DSimulator::update(const math::Vector2d &velocity) {
    position.translation.x += (velocity.x + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaMotion)) * T;
    position.translation.y += (velocity.y + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaMotion)) * T;
}

math::Pose2D Robot2DSimulator::observe() const {
    return math::Pose2D(position.translation.x + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaGPS),
                          position.translation.y + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaGPS));
}

const math::Pose2D &Robot2DSimulator::getPose() const {
    return position;
}
