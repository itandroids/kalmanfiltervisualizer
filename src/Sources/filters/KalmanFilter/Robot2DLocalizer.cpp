//
// Created by mmaximo on 3/16/18.
//

#include "Headers/filters/KalmanFilter/Robot2DLocalizer.h"

Robot2DLocalizer::Robot2DLocalizer(double sigmaMotion, double sigmaGPS, double T)
    :Localizer (){
    Eigen::MatrixXd A(2, 2);
    A << 1.0, 0.0,
            0.0, 1.0;
    Eigen::MatrixXd B(2, 2);
    B << T, 0.0,
            0.0, T;
    Eigen::MatrixXd C(2, 2);
    C << 1.0, 0.0,
            0.0, 1.0;
    Eigen::MatrixXd M(2, 2);
    M << sigmaMotion * sigmaMotion, 0.0,
            0.0, sigmaMotion * sigmaMotion;
    Eigen::MatrixXd Q = B * M * B.transpose();
    Eigen::MatrixXd R(2, 2);
    R << sigmaGPS * sigmaGPS, 0.0,
            0.0, sigmaGPS * sigmaGPS;
    kalmanFilter = std::make_shared<KalmanFilter>(A, B, C, Q, R);
}

void Robot2DLocalizer::reset(const math::Pose2D &resetPosition, const Eigen::MatrixXd &resetCovariance) {
    Eigen::VectorXd x0(2);
    x0 << resetPosition.translation.x, resetPosition.translation.y;
    kalmanFilter->reset(x0, resetCovariance);
}


const math::Pose2D &Robot2DLocalizer::predict(const math::Vector2d &command) {
    Eigen::VectorXd u(2);
    u << command.x, command.y;
    Eigen::VectorXd x = kalmanFilter->predict(u);
    position.translation.x = x(0);
    position.translation.y = x(1);
    return position;
}

const math::Pose2D &Robot2DLocalizer::filter(const math::Pose2D &observation) {
    Eigen::VectorXd z(2);
    z << observation.translation.x, observation.translation.y;
    Eigen::VectorXd x = kalmanFilter->filter(z);
    position.translation.x = x(0);
    position.translation.y = x(1);
    return position;
}

const math::Pose2D &Robot2DLocalizer::getPose() const {
    return position;
}

const Eigen::MatrixXd &Robot2DLocalizer::getCovariance() const {
    return kalmanFilter->getCovariance();
}
