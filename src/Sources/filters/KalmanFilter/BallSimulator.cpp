//
// Created by mmaximo on 3/17/18.
//

#include "Headers/filters/KalmanFilter/BallSimulator.h"
#include "Headers/math/RandomUtils.h"

BallSimulator::BallSimulator(double sigmaAcceleration, double sigmaCamera, double T, int cameraDelay)
        : sigmaAcceleration(sigmaAcceleration), sigmaCamera(sigmaCamera), T(T), cameraDelay(cameraDelay) {
    for (int i = 0; i <= cameraDelay; ++i)
        observations.push(math::Pose2D(position.translation.x + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamera),
                                         position.translation.y + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamera)));
}

void BallSimulator::update() {
    position.translation.x += velocity.translation.x * T;
    position.translation.y += velocity.translation.y * T;
    observations.push(math::Pose2D(position.translation.x + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamera),
                                     position.translation.y + math::RandomUtils::generateGaussianRandomNumber(0.0, sigmaCamera)));
    observations.pop();
}

math::Pose2D BallSimulator::observe() const {
    return observations.front();
}

const math::Pose2D &BallSimulator::getPosition() const {
    return position;
}

const math::Pose2D &BallSimulator::getVelocity() const {
    return velocity;
}

void BallSimulator::setVelocity(const math::Pose2D &velocity) {
    this->velocity = velocity;
}
