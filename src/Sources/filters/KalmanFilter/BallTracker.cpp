//
// Created by mmaximo on 3/17/18.
//

#include "Headers/filters/KalmanFilter/BallTracker.h"

BallTracker::BallTracker(double sigmaAcceleration, double sigmaCamera, double T, int cameraDelay) : cameraDelay(cameraDelay) {
    A.resize(4, 4);
    A << 1.0, 0.0, T, 0.0,
        0.0, 1.0, 0.0, T,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0;
    Eigen::MatrixXd B(4, 2);
    B << T * T / 2.0, 0.0,
        0.0, T * T / 2.0,
        T, 0.0,
        0.0, T;
    Eigen::MatrixXd C(2, 4);
    C << 1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0;
    Eigen::MatrixXd M(2, 2);
    M << sigmaAcceleration * sigmaAcceleration, 0.0,
        0.0, sigmaAcceleration * sigmaAcceleration;
    this->Q = B * M * B.transpose();
    Eigen::MatrixXd R(2, 2);
    R << sigmaCamera * sigmaCamera, 0.0,
        0.0, sigmaCamera * sigmaCamera;
    kalmanFilter = std::make_shared<KalmanFilter>(A, B, C, Q, R);
    kalmanFilter->reset();
}


void BallTracker::resetFilter(const Eigen::VectorXd &resetMean, const Eigen::MatrixXd &resetCovariance) {
    kalmanFilter->reset(resetMean, resetCovariance);
}

void BallTracker::predict() {
    Eigen::VectorXd u = Eigen::VectorXd::Zero(2);
    Eigen::VectorXd x = kalmanFilter->predict(u);
    Eigen::MatrixXd P = kalmanFilter->getCovariance();
    compensateDelayAndAssign(x, P);
}

void BallTracker::filter(const math::Pose2D &observation) {
    Eigen::VectorXd z(2);
    z << observation.translation.x, observation.translation.y;
    Eigen::VectorXd x = kalmanFilter->filter(z);
    Eigen::MatrixXd P = kalmanFilter->getCovariance();
    compensateDelayAndAssign(x, P);
}

void BallTracker::predictAndFilter(const math::Pose2D &observation) {
    Eigen::VectorXd u = Eigen::VectorXd::Zero(2);
    kalmanFilter->predict(u);
    Eigen::VectorXd z(2);
    z << observation.translation.x, observation.translation.y;
    Eigen::VectorXd x = kalmanFilter->filter(z);
    Eigen::MatrixXd P = kalmanFilter->getCovariance();
    compensateDelayAndAssign(x, P);
}

const math::Pose2D &BallTracker::getPosition() const {
    return position;
}

const math::Pose2D &BallTracker::getVelocity() const {
    return velocity;
}

const Eigen::MatrixXd &BallTracker::getCovariance() const {
    return covariance;
}

void BallTracker::compensateDelayAndAssign(Eigen::VectorXd &x, Eigen::MatrixXd &P) {
    for (int i = 0; i < cameraDelay; ++i) {
        x = A * x;
        P = A * P * A.transpose() + Q;
    }
    position.translation.x = x(0);
    position.translation.y = x(1);
    velocity.translation.x = x(2);
    velocity.translation.y = x(3);
    covariance = P;
}
