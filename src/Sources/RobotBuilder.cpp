#include "Headers/RobotBuilder.h"
#include "Headers/Robot.h"
#include "Headers/filters/GpsSensor.h"
#include "Headers/filters/ekf/DDRLocalizer.h"
#include "Headers/filters/ekf/DDRSimulator.h"
#include "Headers/filters/EkfSensor.h"
#include <QtWidgets>

RobotBuilder::RobotBuilder(QGraphicsPathItem *sensorPath,
                           QGraphicsPathItem *filterPath,
                           QGraphicsPathItem *selfPath,
                           QGraphicsItem *parent)
    : RobotBuilderBase(new Robot(sensorPath, filterPath, selfPath, parent))
{

}



KFRobotBuilder RobotBuilderBase::constructKFRobot() const
{
    return KFRobotBuilder(robot);
}

EKFRobotBuilder RobotBuilderBase::constructEKFRobot() const
{
    return EKFRobotBuilder(robot);
}

KFRobotBuilder::KFRobotBuilder(Robot *robot) : RobotBuilderBase(robot)
{

}

KFRobotBuilder::self &KFRobotBuilder::withParameters(double dT, double sigmaGps, double sigmaMotion)
{
   robot->localizer = new Robot2DLocalizer(sigmaMotion, sigmaGps, dT);
   robot->simulator = new Robot2DSimulator(sigmaMotion, sigmaGps, dT);
   robot->sensor = new GpsSensor(dT, sigmaGps, robot);


   robot->latencyTimer = new QTimer(robot);
   QObject::connect(robot->latencyTimer, SIGNAL(timeout()), robot, SLOT(move()));
   int latency = static_cast<int>(round(dT*1000));
   robot->latencyTimer->start(latency);

   QObject::connect(robot->sensor, SIGNAL(infoReady(QList<QVector3D>, QVector3D, QVector3D)),
            robot, SLOT(receiveReading(QList<QVector3D>, QVector3D, QVector3D)));

   auto movementController = [] (Simulator* simulator) {
        static int stepCounter = 0;
        double time = stepCounter*0.1;
        double A = 0.3;
        double B = 0.3;
        double w1 = 1.0;
        double w2 = 2.0*w1/3.0;
        double delta = M_PI/2.0;
        double k = 10;
        math::Vector2d velocity;
        velocity.x = k*A*w1*std::cos(w1*time + delta) ;
        velocity.y = k*B*w2*std::cos(w2*time);
        simulator->update(velocity);
        math::Pose2D pos = simulator->getPose();
        ++stepCounter;
        return pos;
   };

   auto sensorRcvController = [] (Localizer* localizer,
                                   QList<QVector3D> flagsReadings,
                                   QVector3D trackedReading,
                                   QVector3D parentReading) {
        static int stepCounter = 0;
        double time = stepCounter*0.1;
        double A = 0.3;
        double B = 0.3;
        double w1 = 1.0;
        double w2 = 2.0*w1/3.0;
        double delta = M_PI/2.0;
        double k = 10;
        math::Vector2d velocity;
        velocity.x = k*A*w1*std::cos(w1*time + delta) ;
        velocity.y = k*B*w2*std::cos(w2*time);
        localizer->predict(velocity);
        math::Pose2D observation = Robot::qVectorToPose(parentReading);
        localizer->filter(observation);
        math::Pose2D pos = localizer->getPose();
        ++stepCounter;
        return pos;
   };

   robot->moveController = movementController;
   robot->sensorRcvController = sensorRcvController;

   return *this;

}

EKFRobotBuilder::EKFRobotBuilder(Robot *robot) : RobotBuilderBase (robot)
{

}

EKFRobotBuilder::self &EKFRobotBuilder::withParameters(double dT, double sigmaLinearSpeed, double sigmaAngularSpeed, double sigmaCameraCartesian, double sigmaCameraAngle)
{

    robot->localizer = new DDRLocalizer(dT, sigmaLinearSpeed, sigmaAngularSpeed,
                                 sigmaCameraCartesian, sigmaCameraAngle);
    robot->simulator = new DDRSimulator(dT, sigmaLinearSpeed, sigmaAngularSpeed,
                                 sigmaCameraCartesian, sigmaCameraAngle);
    robot->sensor = new EkfSensor(robot->simulator, dT, sigmaCameraCartesian, sigmaCameraAngle,
                           robot);

    math::Pose2D initialPose(50.0 * M_PI / 180.0, 0.0, 0.05);
    Eigen::MatrixXd initialCovariance = 1.0e-3 * Eigen::MatrixXd::Identity(3, 3);
    robot->localizer->reset(initialPose, initialCovariance);

    robot->latencyTimer = new QTimer(robot);
    QObject::connect(robot->latencyTimer, SIGNAL(timeout()), robot, SLOT(move()));
    int latency = static_cast<int>(round(dT*1000));
    robot->latencyTimer->start(latency);

    QObject::connect(robot->sensor, SIGNAL(infoReady(QList<QVector3D>, QVector3D, QVector3D)),
            robot, SLOT(receiveReading(QList<QVector3D>, QVector3D, QVector3D)));

    auto movementController = [] (Simulator* simulator) {
        double linearSpeed = 1.5;
        double angularSpeed = 20.0 * M_PI / 180.0;
        simulator->update({linearSpeed, angularSpeed});
        math::Pose2D pos = simulator->getPose();
        return pos;
   };

   auto sensorRcvController = [] (Localizer* localizer,
                                  QList<QVector3D> flagsReadings,
                                  QVector3D trackedReading,
                                  QVector3D parentReading) {
        double linearSpeed = 1.5;
        double angularSpeed = 20.0 * M_PI / 180.0;
        localizer->predict({linearSpeed, angularSpeed});
        math::Pose2D observation = Robot::qVectorToPose(parentReading);
        localizer->filter(observation);
        math::Pose2D pos = localizer->getPose();
        return pos;
   };

   robot->moveController = movementController;
   robot->sensorRcvController = sensorRcvController;


    return *this;
}




































