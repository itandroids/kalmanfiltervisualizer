﻿#include "Headers/Arrow.h"

#include <QtWidgets>
#include <qmath.h>
#include <QPen>
#include <QPainter>

Arrow::Arrow(QGraphicsItem *origin, QGraphicsItem *parent)
    : QGraphicsLineItem(parent)
{
    myStartItem = origin;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    myColor = Qt::black;
    setPen(QPen(myColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
}

QRectF Arrow::boundingRect() const
{
    qreal extra = (pen().width() + 20) / 2.0;

    return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                      line().p2().y() - line().p1().y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);
}

QPainterPath Arrow::shape() const
{
    QPainterPath path = QGraphicsLineItem::shape();
    path.addPolygon(arrowHead);
    return path;
}

void Arrow::updatePosition()
{
    QLineF line;
    line.setP1(mapFromItem(myStartItem, QPointF(0,0)));
    line.setP2(QPointF(0, 50));
    line.setLength(40.0);
    setLine(line);
}

void Arrow::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
          QWidget *)
{

    QPen myPen = pen();
    myPen.setColor(myColor);
    qreal arrowSize = 10;
    painter->setPen(myPen);
    painter->setBrush(myColor);

//    QLineF centerLine(myStartItem->pos(), myEndItem->pos());
//    QPolygonF endPolygon = myEndItem->polygon();
//    QPointF p1 = endPolygon.first() + myEndItem->pos();
//    QPointF p2;
//    QPointF intersectPoint;
//    QLineF polyLine;
//    for (int i = 1; i < endPolygon.count(); ++i) {
//        p2 = endPolygon.at(i) + myEndItem->pos();
//        polyLine = QLineF(p1, p2);
//        QLineF::IntersectType intersectType =
//            polyLine.intersect(centerLine, &intersectPoint);
//        if (intersectType == QLineF::BoundedIntersection)
//            break;
//        p1 = p2;
//    }

    QLineF line1;
    line1.setP1(mapFromItem(myStartItem, QPointF(0,0)));
    line1.setP2(QPointF(0, 50));
    line1.setLength(40.0);
    //line1.setAngle(myStartItem->rotation());
    setLine(line1);

    double angle = std::atan2(-line().dy(), line().dx());

    QPointF arrowP1 = line().p2() + QPointF(sin(angle - M_PI / 3) * arrowSize,
                                    cos(angle - M_PI / 3) * arrowSize);
    QPointF arrowP2 = line().p2() + QPointF(sin(angle + M_PI + M_PI / 3) * arrowSize,
                                    cos(angle + M_PI + M_PI / 3) * arrowSize);

    arrowHead.clear();
    arrowHead << line().p2() << arrowP1 << arrowP2;
    painter->drawLine(line());
    painter->drawPolygon(arrowHead);
}

void Arrow::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->buttons() & Qt::MouseButton::LeftButton) {
        setCursor(Qt::ClosedHandCursor);
        event->accept();
    }
    //QGraphicsLineItem::mousePressEvent(event);
}

void Arrow::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton)) {
        auto parent = this->parentItem();
        if (parent) {
            auto orig = (parent->pos());
            auto oldP2 = mapToParent(line().p2());
            auto newP2 = mapToParent(event->pos());
            QVector2D v1(oldP2);
            QVector2D v2(newP2);
            if(v2.length() < 25)
                return;
            v1.normalize();
            v2.normalize();
            auto dot = QVector2D::dotProduct(v1, v2);
            auto oldAngle = line().angle();
            auto angle = qAcos(dot)*180.0/M_PI;
            angle = angle > M_PI ? 2*M_PI - angle : angle;
            auto mat = parent->matrix();
            mat.rotate(angle);
            parent->setMatrix(mat);
        }
        event->accept();

    }
}

void Arrow::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->buttons() & Qt::MouseButton::LeftButton) {
        setCursor(Qt::OpenHandCursor);
        event->accept();
    }
}
