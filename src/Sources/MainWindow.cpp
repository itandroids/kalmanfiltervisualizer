#include "../Headers/MainWindow.h"
#include "../Headers/GridGraphicScene.h"
#include "../Headers/View.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    robot = nullptr;
    auto step = 30;
    ui->setupUi(this);
    sideBarLayout = ui->sideBarLayout;
    createSideBar();
    scene = new GridGraphicScene(this);
    auto view = new View("KF");
    //auto view = new QGraphicsView;
    auto w = step*100.0;
    auto h = step*100.0;

    QRect sceneRect(-w/2, -h/2, w, h);

    scene->setSceneRect(sceneRect);

    view->view()->setScene(scene);
    //view->setScene(scene);

    auto canvas = ui->canvasLayout;
    canvas->addWidget(view);

   // ui->graphicsView->fitInView(scene->itemsBoundingRect(), Qt::KeepAspectRatio);
    connect(start, SIGNAL(clicked()), this, SLOT(constructRobot()));
    connect(stop, SIGNAL(clicked()), this, SLOT(stopRobot()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createSideBar()
{
    auto locTrackBox = new QGroupBox(tr("Localizer/Tracking"));
    auto locTracLayout = new QHBoxLayout;
    auto locButton = new QRadioButton;
    locButton->setText(tr("Localizer"));
    locButton->setChecked(true);
    auto ballButton = new QRadioButton;
    ballButton->setText(tr("Ball Tracker"));
    locTracLayout->addWidget(locButton);
    locTracLayout->addWidget(ballButton);
    locTrackBox->setLayout(locTracLayout);

    //Create Localizer page

    auto movementBox = new QGroupBox(tr("Robot Moviment Model"));
    auto movementLayout = new QVBoxLayout;
    robotMovementModelBox = new QComboBox;
    QStringList movementTypes{"Linear", "NonLinear"};
    robotMovementModelBox->addItems(movementTypes);
    movementLayout->addWidget(robotMovementModelBox);
    movementBox->setLayout(movementLayout);

    auto sensorBox = new QGroupBox(tr("Sensor Type"));
    auto sensorLayout = new QVBoxLayout;
    robotSensorTypeBox = new QComboBox;
    QStringList sensorTypes{"Gps Sensor", "Distance/Angle Sensor"};
    robotSensorTypeBox->addItems(sensorTypes);
    sensorLayout->addWidget(robotSensorTypeBox);
    sensorBox->setLayout(sensorLayout);

    auto filterBox = new QGroupBox(tr("Filters Type"));
    auto filterLayout = new QVBoxLayout;
    robotFilterTypeBox = new QComboBox;
    //QStringList filterTypes{"Kalman Filter", "EKF", "UKF", "Particle Filter"};
    QStringList filterTypes{"Kalman Filter", "EKF"};
    robotFilterTypeBox->addItems(filterTypes);
    filterLayout->addWidget(robotFilterTypeBox);
    filterBox->setLayout(filterLayout);

    auto optionsBox = new QGroupBox(tr("Configuration"));
    auto optionsLayout = new QVBoxLayout;
    auto generalLayout = new QGridLayout;
    auto latencyLabel = new QLabel(tr("Latency time: "));
    latencySpin = new QDoubleSpinBox;
    latencySpin->setDecimals(1);
    latencySpin->setValue(0.1);
    latencySpin->setSingleStep(0.1);
    generalLayout->addWidget(latencyLabel, 0, 0);
    generalLayout->addWidget(latencySpin, 0, 1);

    auto kfLayout = new QGridLayout;
    auto sigmaMotionLabel = new QLabel(tr("Sigma Motion: "));
    sigmaMotionSpin = new QDoubleSpinBox;
    sigmaMotionSpin->setDecimals(3);
    sigmaMotionSpin->setValue(0.2);
    sigmaMotionSpin->setSingleStep(0.1);
    auto sigmaGpsLabel = new QLabel(tr("Sigma Gps: "));
    sigmaGpsSpin = new QDoubleSpinBox;
    sigmaGpsSpin->setDecimals(3);
    sigmaGpsSpin->setValue(0.2);
    sigmaGpsSpin->setSingleStep(0.1);
    kfLayout->addWidget(sigmaMotionLabel, 0, 0);
    kfLayout->addWidget(sigmaMotionSpin, 0, 1);
    kfLayout->addWidget(sigmaGpsLabel, 1, 0);
    kfLayout->addWidget(sigmaGpsSpin, 1, 1);
    auto kfWidget = new QWidget;
    kfWidget->setLayout(kfLayout);

    auto ekfLayout = new QGridLayout;
    auto sigmaLinSpd = new QLabel(tr("Sigma Linear Speed: "));
    sigmaLinSpdSpin = new QDoubleSpinBox;
    sigmaLinSpdSpin->setDecimals(3);
    sigmaLinSpdSpin->setValue(0.05);
    sigmaLinSpdSpin->setSingleStep(0.1);
    auto sigmaAng = new QLabel(tr("Sigma Angular Speed: "));
    sigmaAngSpin = new QDoubleSpinBox;
    sigmaAngSpin->setDecimals(3);
    sigmaAngSpin->setValue(0.02);
    sigmaAngSpin->setSingleStep(0.1);
    auto sigmaCamCart = new QLabel(tr("Sigma Camera Cartesian: "));
    sigmaCamCartSpin = new QDoubleSpinBox;
    sigmaCamCartSpin->setDecimals(3);
    sigmaCamCartSpin->setValue(0.1);
    sigmaCamCartSpin->setSingleStep(0.1);
    auto sigmaCamAng = new QLabel(tr("Sigma Camera Angle: "));
    sigmaCamAngSpin = new QDoubleSpinBox;
    sigmaCamAngSpin->setDecimals(3);
    sigmaCamAngSpin->setValue(0.04);
    sigmaCamAngSpin->setSingleStep(0.1);
    ekfLayout->addWidget(sigmaLinSpd, 0, 0);
    ekfLayout->addWidget(sigmaLinSpdSpin, 0, 1);
    ekfLayout->addWidget(sigmaAng, 1, 0);
    ekfLayout->addWidget(sigmaAngSpin, 1, 1);
    ekfLayout->addWidget(sigmaCamCart, 2, 0);
    ekfLayout->addWidget(sigmaCamCartSpin, 2, 1);
    ekfLayout->addWidget(sigmaCamAng, 3, 0);
    ekfLayout->addWidget(sigmaCamAngSpin, 3, 1);
    auto ekfWidget = new QWidget;
    ekfWidget->setLayout(ekfLayout);

    auto ukfLayout = new QGridLayout;
    auto ukfSigmaLinSpd = new QLabel(tr("Sigma Linear Speed: "));
    ukfSigmaLinSpdSpin = new QDoubleSpinBox;
    ukfSigmaLinSpdSpin->setDecimals(3);
    ukfSigmaLinSpdSpin->setValue(0.2);
    ukfSigmaLinSpdSpin->setSingleStep(0.1);
    auto ukfSigmaAng = new QLabel(tr("Sigma Angular Speed: "));
    ukfSigmaAngSpin = new QDoubleSpinBox;
    ukfSigmaAngSpin->setDecimals(3);
    ukfSigmaAngSpin->setValue(0.2);
    ukfSigmaAngSpin->setSingleStep(0.1);
    auto ukfSigmaCamCart = new QLabel(tr("Sigma Camera Cartesian: "));
    ukfSigmaCamCartSpin = new QDoubleSpinBox;
    ukfSigmaCamCartSpin->setDecimals(3);
    ukfSigmaCamCartSpin->setValue(0.2);
    ukfSigmaCamCartSpin->setSingleStep(0.1);
    auto ukfSigmaCamAng = new QLabel(tr("Sigma Camera Angle: "));
    ukfSigmaCamAngSpin = new QDoubleSpinBox;
    ukfSigmaCamAngSpin->setDecimals(3);
    ukfSigmaCamAngSpin->setValue(0.2);
    ukfSigmaCamAngSpin->setSingleStep(0.1);
    ukfLayout->addWidget(ukfSigmaLinSpd, 0, 0);
    ukfLayout->addWidget(ukfSigmaLinSpdSpin, 0, 1);
    ukfLayout->addWidget(ukfSigmaAng, 1, 0);
    ukfLayout->addWidget(ukfSigmaAngSpin, 1, 1);
    ukfLayout->addWidget(ukfSigmaCamCart, 2, 0);
    ukfLayout->addWidget(ukfSigmaCamCartSpin, 2, 1);
    ukfLayout->addWidget(ukfSigmaCamAng, 3, 0);
    ukfLayout->addWidget(ukfSigmaCamAngSpin, 3, 1);

    auto ukfWidget = new QWidget;
    ukfWidget->setLayout(ukfLayout);

    auto pfLayout = new QGridLayout;
    auto pfSigmaLinSpd = new QLabel(tr("Sigma Linear Speed: "));
    pfSigmaLinSpdSpin = new QDoubleSpinBox;
    pfSigmaLinSpdSpin->setDecimals(3);
    pfSigmaLinSpdSpin->setValue(0.2);
    pfSigmaLinSpdSpin->setSingleStep(0.1);
    auto pfSigmaAng = new QLabel(tr("Sigma Angular Speed: "));
    pfSigmaAngSpin = new QDoubleSpinBox;
    pfSigmaAngSpin->setDecimals(3);
    pfSigmaAngSpin->setValue(0.2);
    pfSigmaAngSpin->setSingleStep(0.1);
    auto pfSigmaCamCart = new QLabel(tr("Sigma Camera Cartesian: "));
    pfSigmaCamCartSpin = new QDoubleSpinBox;
    pfSigmaCamCartSpin->setDecimals(3);
    pfSigmaCamCartSpin->setValue(0.2);
    pfSigmaCamCartSpin->setSingleStep(0.1);
    auto pfSigmaCamAng = new QLabel(tr("Sigma Camera Angle: "));
    pfSigmaCamAngSpin = new QDoubleSpinBox;
    pfSigmaCamAngSpin->setDecimals(3);
    pfSigmaCamAngSpin->setValue(0.2);
    pfSigmaCamAngSpin->setSingleStep(0.1);
    pfLayout->addWidget(pfSigmaLinSpd, 0, 0);
    pfLayout->addWidget(pfSigmaLinSpdSpin, 0, 1);
    pfLayout->addWidget(pfSigmaAng, 1, 0);
    pfLayout->addWidget(pfSigmaAngSpin, 1, 1);
    pfLayout->addWidget(pfSigmaCamCart, 2, 0);
    pfLayout->addWidget(pfSigmaCamCartSpin, 2, 1);
    pfLayout->addWidget(pfSigmaCamAng, 3, 0);
    pfLayout->addWidget(pfSigmaCamAngSpin, 3, 1);

    auto pfWidget = new QWidget;
    pfWidget->setLayout(pfLayout);


    localizerConfigStack = new QStackedWidget;
    localizerConfigStack->addWidget(kfWidget);
    localizerConfigStack->addWidget(ekfWidget);
    localizerConfigStack->addWidget(ukfWidget);
    localizerConfigStack->addWidget(pfWidget);
    optionsLayout->addLayout(generalLayout);
    optionsLayout->addWidget(localizerConfigStack);
    connect(robotFilterTypeBox, SIGNAL(activated(int)),
            localizerConfigStack, SLOT(setCurrentIndex(int)));


    optionsBox->setLayout(optionsLayout);

    auto localizerLayout = new QVBoxLayout;

    localizerLayout->addWidget(filterBox);
    localizerLayout->addWidget(movementBox);
    movementBox->setEnabled(false);
    localizerLayout->addWidget(sensorBox);
    sensorBox->setEnabled(false);
    localizerLayout->addWidget(optionsBox);
    // Create Ball Page

    auto ballMovementBox = new QGroupBox(tr("Ball Moviment Model"));
    auto ballMovementLayout = new QVBoxLayout;
    ballMovementModelBox = new QComboBox;
    QStringList ballMovementTypes{"Linear", "NonLinear"};
    ballMovementModelBox->addItems(ballMovementTypes);
    ballMovementLayout->addWidget(ballMovementModelBox);
    ballMovementBox->setLayout(ballMovementLayout);

    auto ballSensorBox = new QGroupBox(tr("Sensor Type"));
    auto ballSensorLayout = new QVBoxLayout;
    ballSensorTypeBox = new QComboBox;
    QStringList ballSensorTypes{"Gps Sensor", "Distance/Angle Sensor"};
    ballSensorTypeBox->addItems(ballSensorTypes);
    ballSensorLayout->addWidget(ballSensorTypeBox);
    ballSensorBox->setLayout(ballSensorLayout);

    auto ballFilterBox = new QGroupBox(tr("Filters Type"));
    auto ballFilterLayout = new QVBoxLayout;
    ballFilterTypeBox = new QComboBox;
    QStringList ballFilterTypes{"Kalman Filter", "UKF", "Particle Filter"};
    ballFilterTypeBox->addItems(ballFilterTypes);
    ballFilterLayout->addWidget(ballFilterTypeBox);
    ballFilterBox->setLayout(ballFilterLayout);

    auto ballOptionsBox = new QGroupBox(tr("Configuration"));
    auto ballOptionsLayout = new QVBoxLayout;
    auto ballGeneralLayout = new QGridLayout;
    optionsLayout->addLayout(ballGeneralLayout);


    optionsBox->setLayout(optionsLayout);

    auto ballLayout = new QVBoxLayout;

    ballLayout->addWidget(ballMovementBox);
    ballLayout->addWidget(ballSensorBox);
    ballLayout->addWidget(ballFilterBox);
    ballLayout->addWidget(ballOptionsBox);

    localizerTrackerStack = new QStackedWidget;
    auto localizerWidget = new QWidget;
    localizerWidget->setLayout(localizerLayout);
    localizerTrackerStack->addWidget(localizerWidget);
    auto ballWidget = new QWidget;
    ballWidget->setLayout(ballLayout);
    localizerTrackerStack->addWidget(ballWidget);
    ballWidget->setEnabled(false);

    auto runBox = new QGroupBox(tr("Run/Stop"));
    auto runLayout = new QHBoxLayout;
    start = new QPushButton("Start");
    stop = new QPushButton("Stop");
    runLayout->addWidget(start);
    runLayout->addWidget(stop);
    runBox->setLayout(runLayout);

    sideBarLayout->addWidget(locTrackBox);
    sideBarLayout->addWidget(localizerTrackerStack);
    sideBarLayout->addWidget(runBox);

    auto signalMapper = new QSignalMapper(this);
    connect(locButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ballButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(locButton, 0);
    signalMapper->setMapping(ballButton, 1);

    connect(signalMapper, SIGNAL(mapped(int)),
            localizerTrackerStack, SLOT(setCurrentIndex(int)));

}

void MainWindow::constructRobot()
{
    if (robot) {
        scene->removeItem(robot);
        //delete robot;
    }
    if (sensorPath)
        scene->removeItem(sensorPath);
    if (sensorPath)
        delete sensorPath;
    if (filterPath)
        scene->removeItem(filterPath);
    if (filterPath)
        delete filterPath;
    if (robotPath)
        scene->removeItem(robotPath);
    if (robotPath)
        delete robotPath;
    sensorPath = new QGraphicsPathItem;
    sensorPath->setPen(QPen(Qt::blue));
    QPainterPath sensorPP;
    sensorPP.moveTo({0, 0});
    sensorPath->setPath(sensorPP);
    scene->addItem(sensorPath);

    filterPath = new QGraphicsPathItem;
    auto pen = QPen(Qt::red);
    //pen.setStyle(Qt::DotLine);
    filterPath->setPen(pen);
    QPainterPath filterPP;
    filterPP.moveTo({0, 0});
    filterPath->setPath(filterPP);
    scene->addItem(filterPath);

    robotPath = new QGraphicsPathItem;
    QPainterPath robotPP;
    robotPP.moveTo({0, 0});
    robotPath->setPath(robotPP);
    scene->addItem(robotPath);

    auto filterIndex = robotFilterTypeBox->currentIndex();

    if (filterIndex == 0) {
       robot = Robot::create(sensorPath, filterPath, robotPath)
               .constructKFRobot()
               .withParameters(latencySpin->value(),
                               sigmaGpsSpin->value(),
                               sigmaMotionSpin->value());
    } else {
        robot = Robot::create(sensorPath, filterPath, robotPath)
                .constructEKFRobot()
                .withParameters(latencySpin->value(),
                                sigmaLinSpdSpin->value(),
                                sigmaAngSpin->value(),
                                sigmaCamCartSpin->value(),
                                sigmaCamAngSpin->value());
    }
    robot->setTransform(QTransform::fromScale(1.2, 1.2), true);
    robot->setPos(0, 0);
    scene->addItem(robot);
    start->setDown(true);
    localizerTrackerStack->setEnabled(false);

}

void MainWindow::stopRobot()
{
    if(robot)
        scene->removeItem(robot);
    if(robot)
        delete robot;
    robot = nullptr;
    start->setDown(false);
    localizerTrackerStack->setEnabled(true);
}


















































