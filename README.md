# KalmanFilterVisualizer 


Localizer and Tracking Visualizer using Kalman Filters. Used in ITAndroids signal processing course.

## Installing / Getting started

A quick introduction of the minimal setup you need to get a hello world up &
running.

```shell
commands here
```

Here you should say what actually happens when you execute the code above.

## Developing

### Built With
QT5



### Building

If your project needs some additional steps for the developer to build the
project after some code changes, state them here. for example:

```shell
git clone https://vasconssa@bitbucket.org/itandroids/kalmanfiltervisualizer.git
cd kalmanfiltervisualizer/
mkdir build && cd build
cd build
cmake ..
make
```

Here again you should state what actually happens when the code above gets
executed.



## Licensing

State what the license is and how to find the text version of the license.


